# React Typescript

Build with React, Typescript and Webpack 4.

* HMR for development
* JavaScript compiles down to ES5

## Instructions

1. Install dependencies: `npm install`
2. Compile: `npm compile`
3. Run development environment: `npm run dev`

## Licence

Licensed under [MIT](https://opensource.org/licenses/MIT) licence.